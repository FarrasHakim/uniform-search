from django.shortcuts import render
from nodes.models import Node
# Create your views here.
def index(request):
	return render(request, "home.html")

def cari_rute(request):
	all_node = Node.objects.order_by("name")
	context = {}
	context["nodes"] = all_node
	return render(request, "cari-rute.html", context)
