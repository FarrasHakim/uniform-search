# UI MAPS

## Kelompok Ai-Kun
#### Anggota:
- Edward Partogi Gembira Abyatar (1706979215)
- Muhammad Farras Hakim (1706024513)
- Nur Nisrina Ningrum (1706979423)
- Revan Ragha Andhito (1706039420)

## Overview
UI Maps merupakan proyek web-based application yang akan membantu pengguna untuk menemukan rute terdekat dari suatu lokasi menuju lokasi lain yang berada di Universitas Indonesia dengan memilih pilihan dengan berjalan kaki atau dengan kendaraan roda dua atau empat.

## How to Run
Kunjungi UI Maps pada [uimaps.herokuapp.com](https://uimaps.herokuapp.com) 