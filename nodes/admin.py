from django.contrib import admin
from .models import *
# Register your models here.

class NodeAdmin(admin.ModelAdmin):
	list_display = ["name"]
	list_filter = ["name"]
	search_fields = ["name"]

	class Meta:
		model = Node

class DistanceAdmin(admin.ModelAdmin):
	list_display = ["nodeFrom", "nodeTo","distance"]
	list_filter = ["nodeFrom", "nodeTo", "distance"]
	search_fields = ["nodeFrom", "nodeTo"]

	class Meta:
		model = Distance

admin.site.register(Node, NodeAdmin)
admin.site.register(Distance, DistanceAdmin)
