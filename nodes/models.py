from django.db import models

# Create your models here

class Node(models.Model):
	name = models.CharField(max_length=255)
	def __str__(self):
		return '{}'.format(self.name)

	def getSuccessors(tipeJarak, nodeFrom):
		Distance.objects.get(tipeJarak=tipeJarak, nodeFrom=nodeFrom)

class Distance(models.Model):
	opsi = [
		("Jalan","Jalan"),
		("Kendaraan", "Kendaraan")
	]
	tipeJarak = models.CharField(max_length=12, choices=opsi, default="Jalan")
	nodeFrom = models.ForeignKey(Node, related_name="NodeFrom", on_delete=models.CASCADE)
	nodeTo = models.ForeignKey(Node, related_name="NodeTo", on_delete=models.CASCADE)
	distance = models.IntegerField()
	def __str__(self):
		return '{} to {}'.format(self.nodeFrom, self.nodeTo)
	def get_dataStr(self):
                return self.nodeFrom.__str__() + " " + self.nodeTo.__str__() + " " + str(self.distance) + "\n"
