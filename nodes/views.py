from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .models import Node, Distance
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import heapq, time, json

@csrf_exempt
def index(request):
    if request.method == "POST":
        print(request.POST)
        json_dict = {}
        json_dict["data"] = request["POST"]
        return  JsonResponse(json_dict)
    else:
        global data_nodes
        data_nodes = ""
        for pair in Distance.objects.all():
            data_nodes += pair.get_dataStr()
        
        run_algo(request.GET["node_start"], request.GET["node_end"])
        
        my_dict = {}
        my_dict["nodes"] = optimized_path
        return JsonResponse(my_dict)
    return render(request, "node_index.html")

def getNodes(request):
    nodes_queryset = Node.objects.order_by("name")
    nodes_list = []
    for node in nodes_queryset:
        nodes_list.append(node.name)
    distances_list = []
    for distance in Distance.objects.all():
        distances_list.append({"nodeFrom" : distance.nodeFrom.name, "nodeTo": distance.nodeTo.name})

    return JsonResponse({ "nodes" : nodes_list, "distances" : distances_list})

import sys

# reference : https://github.com/sumedhvdatar/UniformCostSearch/blob/master/find_route1.py

def init_variables(node_start, node_end):
    global source_location
    global destination_location
    global optimized_path
    global queue
    global location_list_from_destination_to_source
    global location_list_to_backtrack
    global visited_list
    global from_list
    global unique_list_of_location_names
    
    unique_list_of_location_names = []
    from_list = []
    visited_list = []
    location_list_to_backtrack = []
    location_list_from_destination_to_source = []
    queue = {}
    optimized_path = []
    source_location = node_start
    destination_location = node_end

def backtrack_to_get_optimized_path(last_pair):
    connected_pair = None
    get_parent_from_child = last_pair.split("|")[0]
    for visited_pair in visited_list:
        if(get_parent_from_child == visited_pair.split("|")[1]):
            break
    if(visited_pair.split("|")[0] == source_location):
        optimized_path.append(visited_pair)
    else:
        optimized_path.append(visited_pair)
        backtrack_to_get_optimized_path(visited_pair)

def find_path(source,destination,distance_to_add):
    parent = 0
    for line in data_nodes.split("\n"):
        if line == "": continue
        
        convert_line_to_array = line.split(" ")
        location_1 = convert_line_to_array[0]
        location_2 = convert_line_to_array[1]
        distance = int(convert_line_to_array[2]) + distance_to_add

        if (location_1 == source):
            delete = False
            for visited_node in visited_list:
                visited_node = visited_node.split("|")[0]
            
                if(visited_node == location_2):
                    delete = True
            if delete == False:
                key = source+"|"+location_2+"|"+str(distance)+"|"+convert_line_to_array[2]
                queue[key] = distance
            
        elif (location_2 == source):
            delete = False
            for visited_node in visited_list:
                visited_node = visited_node.split("|")[0]
                if (visited_node == location_1):
                    delete = True
            if(delete == False):
                key = source+"|"+location_1+"|"+str(distance)+"|"+convert_line_to_array[2]
                queue[key] = distance
                
    if queue == {}:
        print("Distance:Unknown")
        print("Route : None")
    else:
        contents = sorted(queue, key=queue.get)[0]
        del queue[contents]
        parent = contents.split("|")[0]
        location_to_be_sent = contents.split("|")[1]
        distance_to_be_sent = contents.split("|")[2]
        actual_distance = contents.split("|")[3]
        visited_list.append(contents)

        if (location_to_be_sent == destination):
            optimized_path.append(contents)
            backtrack_to_get_optimized_path(contents)
        else:
             find_path(location_to_be_sent,destination_location, int(distance_to_be_sent))

def run_algo(node_start, node_end):
    init_variables(node_start, node_end)
    find_path(source_location,destination_location,0)
