var nonactiveimg = "https://i.ibb.co/7Vvxd56/place-nonactive.png";

var nodesArray = [];
var edgesArray = [];
var network = null;

// Called when the Visualization API is loaded.
function startNetwork(){
  $.ajax({
      // create nodes.
      // value corresponds with the age of the person
      url: '/nodes/datas/',
      type: 'GET',
      success: function(response) {
          // create node
          for (var i = 0; i < response.nodes.length; i++) {
              nodesArray[i] = { id: i, shape: "circularImage", image: nonactiveimg, label: response.nodes[i]};
          }
          // console.log(nodesArray)
          nodes = new vis.DataSet(nodesArray);

          // create connections between nodes
          // value corresponds with node's id that edges between two nodes are defined
          for (var j = 0; j < response.distances.length; j++) {
              var node;
              var toId;
              var toId;
              for (node of nodesArray) {
                  if (node.label == response.distances[j].nodeFrom) {
                      fromId = node.id;
                  }
                  if (node.label == response.distances[j].nodeTo) {
                      toId = node.id;
                  }
              }
              edgesArray[j] = { from: fromId, to: toId };
          }
          edges = new vis.DataSet(edgesArray);

          // create network, nodes with edges
          var container = document.getElementById("mynetwork");
          var data = {
              nodes: nodes,
              edges: edges
          };
          var options = {
              nodes: {
                  size: 30,
                  font: "20px Poppins #828282"
              },
              edges: {
                  color: "#BDBDBD"
              },
              interaction: {
                navigationButtons: true,
                keyboard: true
              }
              
          };
          network = new vis.Network(container, data, options);
      }
  });
}

function changeToActiveNode(param) {
  var activeimg = "https://i.ibb.co/gwR75BX/place-active.png"
  nodes.update([{id: param, image: activeimg}])
}

function changeToActiveEdge(param1, param2) {
  edges.update([{from: param1, to: param2, color: "#301551"}])
}

function resetData() {
  nodes = new vis.DataSet(nodesArray);
  edges = new vis.DataSet(edgesArray);
  network.setData({ nodes: nodes, edges: edges });
}

$(function() {
  $("#cariRuteForm").on('submit', function(e) {
    e.preventDefault();
    resetData();
    $.ajax({
          url : '/nodes/',
          type: 'GET',
          data: {
              node_start : $('#inputAsal').val(),
              node_end : $('#inputTujuan').val(),
          },
          success : function(response) {
            var ulist = ""
            var jarak = 0
            if (response.nodes.length == 0) {
              // Input with same source and destination
              var sameNode = $('#inputTujuan').val()
              ulist += '<li class="list-item">'+ sameNode +'</li>'

              // Change image for selected path
              for (var j = 0; j < nodesArray.length; j++){
                if (nodesArray[j].label == sameNode){
                  changeToActiveNode(j);
                }
              }   
            } else if (response.nodes.length == 2) {
              
              var response_arr = response.nodes[0].split("|")
              var response_arr2 = response.nodes[1].split("|")

              if (response_arr[0] == response_arr2[0]) { // One edges path
                ulist += '<li class="list-item">'+ response_arr[0] +'</li>'
                    + '<li class="list-item">'+ response_arr[1] +'</li>';
                jarak = response_arr[2]
                // Change image for selected path               
                for (var j = 0; j < nodesArray.length; j++){
                  var src = response_arr[0]
                  var dest = response_arr[1]
                  if (nodesArray[j].label == src ||
                    nodesArray[j].label == dest ){
                    changeToActiveNode(j);
                  }
                }                
              } else { // Two edges path
                ulist += '<li class="list-item">'+ response_arr2[0] +'</li>'
                    + '<li class="list-item">'+ response_arr2[1] +'</li>'
                    + '<li class="list-item">'+ response_arr[1] +'</li>';
                jarak = response_arr[2]
                // Change image for selected path
                for (var j = 0; j < nodesArray.length; j++){
                  if (nodesArray[j].label == response_arr2[0] ||
                    nodesArray[j].label == response_arr2[1] ||
                    nodesArray[j].label == response_arr[1] ){
                    changeToActiveNode(j);
                  }
                }
              }
            } else {
              // Get total shortest distance
              jarak = response.nodes[0].split("|")[2]

              for (var i = response.nodes.length-1; i >= 0; i--) {
                var temp_arr = response.nodes[i].split("|");  
                if (i == response.nodes.length-1) {
                    // Return first and second nodes 
                    ulist += '<li class="list-item">'+ temp_arr[0] +'</li>'
                    + '<li class="list-item">'+ temp_arr[1] +'</li>';

                    // Change image for selected path
                    for (var j = 0; j < nodesArray.length; j++){
                       if (nodesArray[j].label == temp_arr[0] ||
                          nodesArray[j].label == temp_arr[1]){
                        changeToActiveNode(j);
                       }
                    }
                } else {
                  // Return the next internal nodes
                  ulist += '<li class="list-item">'+ temp_arr[1] +'</li>';

                  // Change image for selected path
                  for (var j = 0; j < nodesArray.length; j++){
                    if (nodesArray[j].label == temp_arr[1]){
                      changeToActiveNode(j);
                    }
                  }
                }
              }              
            }
            $("#listRute").html(ulist);
            $("#jarakRute").html(jarak + " meter");
          }
      });
  });
});

startNetwork();
