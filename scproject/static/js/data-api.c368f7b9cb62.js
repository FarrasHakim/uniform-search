$(function() {
	$("#cariRuteForm").on('submit', function(e) {
		e.preventDefault();
		
		$.ajax({
	        url : '/nodes/',
	        type: 'GET',
	        data: {
	            node_start : $('#inputAsal').val(),
	            node_end : $('#inputTujuan').val(),
	        },
	        success : function(response) {
	        	var ulist = ""
	        	var jarak = 0
	        	if (response.nodes.length == 0) {
	        		ulist += $('#inputTujuan').val()	        		
	        	} else if (response.nodes.length == 2) {
	        		var response_arr = response.nodes[0].split("|")
	        		var response_arr2 = response.nodes[1].split("|")

	        		if (response_arr[0] == response_arr2[0]) {

	        			ulist += '<li class="list-item">'+ response_arr[0] +'</li>'
	        					+ '<li class="list-item">'+ response_arr[1] +'</li>';
	        			
	        			jarak = response_arr[2]
	        		} else {
	        			ulist += '<li class="list-item">'+ response_arr2[0] +'</li>'
	        					+ '<li class="list-item">'+ response_arr2[1] +'</li>'
	        					+ '<li class="list-item">'+ response_arr[1] +'</li>';
	        			jarak = response_arr[2]
	        		}
	        	} else {
	        		jarak = response.nodes[0].split("|")[2]
		        	for (var i = response.nodes.length-1; i >= 0; i--) {
		        		var temp_arr = response.nodes[i].split("|")
		        		if (i == response.nodes.length-1) {
		        			ulist += '<li class="list-item">'+ temp_arr[0] +'</li>'
	        					+ '<li class="list-item">'+ temp_arr[1] +'</li>';
		        		} else {
		        			ulist += '<li class="list-item">'+ temp_arr[1] +'</li>';
		        		}
		        	}	        		
	        	}
	        	$("#listRute").html(ulist);
	        	$("#jarakRute").html(jarak + " meter");
	        }
	    });
	});


});
